__author__ = 'SoYoung Lee'

import GameFramework
import LevelState

from pico2d import *
from Pointer import Pointer


name = "ScoreState"
image = None
star1, star2, star3 = None, None, None
score_star = None
score_font = None
sound = None
pointer = None
click_sound = None

def enter():
    from Main import score
    global image, star1, star2, star3, sound, score_star, score_font, pointer,click_sound

    score_star = score
    score_font = load_font("resources/lazy_sunday_regular.ttf", 80)
    image = load_image('resources/score_background.png')
    click_sound = load_wav('sound/click.wav')
    sound = load_wav('sound/goal.wav')
    sound.play()
    pointer = Pointer()

    if score_star > 200:
        star1 = load_image('resources/star_filled.png')
        star2 = load_image('resources/star_filled.png')
        star3 = load_image('resources/star_filled.png')

    elif 100 < score_star and score_star <= 200:
        star1 = load_image('resources/star_filled.png')
        star2 = load_image('resources/star_filled.png')
        star3 = load_image('resources/star_empty.png')

    elif 0 <= score_star and score_star <= 100:
        star1 = load_image('resources/star_filled.png')
        star2 = load_image('resources/star_empty.png')
        star3 = load_image('resources/star_empty.png')

def exit():
    global image, star1, star2, star3,score_font,sound,pointer,click_sound
    del(image)
    del(star1)
    del(star2)
    del(star3)
    del(score_font)
    del(click_sound)
    del(sound)
    del(pointer)


def handle_events(frame_time):
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            GameFramework.quit()
        else:
            if event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
                exit()
            elif event.type == SDL_MOUSEBUTTONDOWN and event.button == SDL_BUTTON_LEFT:
                click_sound.play()
                delay(0.2)
                GameFramework.change_state(LevelState)
            elif event.type == SDL_MOUSEMOTION:
                pointer.x, pointer.y = event.x, 599 - event.y


def draw(frame_time):
    global image, star1, star2, star3, score_font, score_star,pointer
    clear_canvas()
    image.draw(400,300)
    star1.draw(300,340)
    star2.draw(400,390)
    star3.draw(500,340)
    score_font.draw(340,200, '%d' % score_star, (217,103,77))
    pointer.draw()
    update_canvas()




def update(frame_time):
    global pointer
   # pointer.update(frame_time)


def pause():
    pass


def resume():
    pass






