__author__ = 'SoYoung Lee'

import GameFramework
import Main

from pico2d import *
from Pointer import Pointer



name = "LevelState"
image = None
import_level = None
level_map = None
level_select = None
level_lock = None
pointer = None
click_sound = None


def enter():
    from Main import map
    global image, import_level, level_select, level_lock, pointer, level_map,click_sound

    image = load_image('resources/level_background.png')
    level_select = load_image('resources/level_select.png')
    level_lock =  load_image('resources/level_lock2.png')
    click_sound = load_wav('sound/click.wav')
    level_map = map

    if level_map is None:
        import_level = 1
    else:
        import_level = level_map.level
    pointer = Pointer()


def exit():
    global image,map, level_select, level_lock, pointer
    del(image)
    del(level_select)
    del(level_lock)
    del(pointer)


def handle_events(frame_time):
    events = get_events()
    for event in events:
        if event.type == SDL_QUIT:
            GameFramework.quit()
        else:
            if( event.type, event.key ) == (SDL_KEYDOWN, SDLK_ESCAPE):
                GameFramework.quit()
            elif event.type == SDL_MOUSEBUTTONDOWN and event.button == SDL_BUTTON_LEFT:
                click_sound.play()
                check_level(event.x, 599 - event.y)
            elif event.type == SDL_MOUSEMOTION:
                pointer.x, pointer.y = event.x, 599 - event.y


def draw(frame_time):
    clear_canvas()
    image.draw(400,300)
    level_select.draw(400,300)
    if import_level != 5:
        level_lock.clip_draw( import_level*120, 0, 600-import_level*120, 100, 400 + import_level*60, 300)
    pointer.draw()
    update_canvas()


def update(frame_time):
    pass


def pause():
    pass


def resume():
    pass

def check_level(x, y):
    print(x, y)
    global level_map
    if 250 <= y and y <= 350:
        if 100 <= x and x <= 220:
            if level_map is not None:
                level_map.curr_level = 1
                GameFramework.pop_state()
            else:
                GameFramework.change_state(Main)

        elif 220 < x and x <= 340:
            temp_level = 2
            if temp_level <= level_map.level:
                level_map.curr_level = 2
                GameFramework.pop_state()

        elif 340 < x and x <= 460:
            temp_level = 3
            if temp_level <= level_map.level:
                level_map.curr_level = 3
                GameFramework.pop_state()

        elif 460 < x and x <= 580:
            temp_level = 4
            if temp_level <= level_map.level:
                level_map.curr_level = 4
                GameFramework.pop_state()

        elif 580 < x and x <= 700:
            temp_level = 5
            if temp_level <= level_map.level:
                level_map.curr_level = 5
                GameFramework.pop_state()






