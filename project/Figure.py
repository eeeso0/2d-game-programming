__author__ = 'SoYoung Lee'
from pico2d import*

class Figure:
    image = None
    def __init__(self):
        self.x = 400
        self.y = 300
        self.state = 1

        if self.image == None:
            self.image = load_image("resources/figure.png")

    def set_position(self, a, b):
        self.x = a*50 + 300
        self.y = b*50 + 200

    def update(self, frame_time):
        pass

    def draw(self):
        if self.state == 0:
            self.image.clip_draw(55, 0, 55, 55, self.x, self.y)
        elif self.state == 1:
            self.image.clip_draw(110, 0, 55, 55, self.x, self.y)