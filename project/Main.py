__author__ = 'SoYoung Lee'

from pico2d import *
from Figure import Figure
from Pointer import Pointer
from Map import Map
from UI import UI
import GameFramework
import ScoreState


name = "MainState"

################################GLOBAL#############################

bg_image = None
ui = None
goal_sound = None
click_sound = None
pointer = None
figure = None
map = None

score = 0
curr_time = 0
timer = 0
click_num = 0
finish = False

####################################################################

def create_world():
    global figure, map, pointer, bg_image, goal_sound, click_sound, ui
    pointer = Pointer()
    figure = [[Figure() for i in range(5)] for j in range(5)]
    map = Map()
    ui = UI()
    bg_image = load_image('resources/background.png')
    goal_sound = load_wav('sound/goal.wav')
    click_sound = load_wav('sound/click.wav')

def destroy_world():
    global figure, map, pointer, ui
    del(ui)
    del(figure)
    del(map)
    del(pointer)

def enter():
    global ui
    create_world()
    ui.time_now = get_time()
    map.set_map()
    hide_cursor()
    for j in range(5):
        for i in range(5):
            figure[j][i].set_position(i,j)

def exit(frame_time):
   destroy_world()
   close_canvas()

def update(frame_time):
    global  finish
    if finish is False:
        pointer.update(frame_time)
        ui.update()

def draw(frame_time):
    clear_canvas()
    bg_image.draw(400,300)
    map.draw()
    ui.draw()
    for j in range(5):
         for i in range(5):
            figure[j][i].draw()

    pointer.draw()
    update_canvas()
    check_goal()
    delay(0.05)

def handle_events(frame_time):
    global pointer
    global figure
    events = get_events()

    for event in events:
        if event.type == SDL_QUIT:
            exit(frame_time)
        elif event.type == SDL_KEYDOWN and event.key == SDLK_ESCAPE:
            exit(frame_time)
        elif event.type == SDL_MOUSEMOTION:
            pointer.x, pointer.y = event.x, 599 - event.y
        elif event.type == SDL_KEYDOWN and event.key == SDLK_RIGHT: # 테스트시 사용 다음 레벨로 넘어가게
            load_next_level()
        elif event.type == SDL_KEYDOWN and event.key == SDLK_r: #reset
            init_state()
        elif event.type == SDL_MOUSEBUTTONDOWN and event.button == SDL_BUTTON_LEFT:
            click_sound.play()
            check_collision(event.x, 599-event.y)
            #print("click : ", click_num)

def pause():
    pass


def resume():
    global figure, ui, map

    #도형 초기화
    for j in range(5):
        for i in range(5):
            figure[j][i].state = 1

    #맵 설정
    map.set_map()


    #클릭 수, 시간 초기화
    ui.__init__()


#########################################################################################################################

def load_next_level():
    global map, finish, ui, figure, score

    #점수 출력
    score = 300 + map.curr_level*10 - ui.click*2 - ui.timer
    if score < 0:
        score = 0
    print("Score : ", score)

    #새로운 레벨 확장
    if map.curr_level == map.level and map.level < 5:
        map.level  = map.level + 1

    GameFramework.push_state(ScoreState)



def init_state():
    global figure

    for j in range(5):
        for i in range(5):
            figure[j][i].state = 1


def change_state( a, b):
    global figure

    for j in range(3):
         for i in range(3):
             I, J = a+i-1, b+j-1
             if I > -1 and I < 5 and J > -1 and J < 5:
                 figure[J][I].state = (figure[J][I].state + 1 ) % 2

def check_collision( a, b):
    global figure, ui

    for J in range(5):
        for I in range(5):
             if abs(figure[J][I].x - a) < 15 and abs(figure[J][I].y - b) < 15:
                  change_state(I,J)
                  ui.click = ui.click + 1


def check_goal():
    global figure, map, timer

    check_num = 0

    for j in range(5):
        for i in range(5):
            if figure[j][i].state != map.goal[j][i]:      #다를 경우 체크
                check_num += 1

   # print("num : ", num)
    if check_num == 0:     #모두 일치
        timer = get_time() - curr_time
        print("Time : ", timer)
        load_next_level()


