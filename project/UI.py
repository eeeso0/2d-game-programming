__author__ = 'SoYoung Lee'

import Main
from pico2d import*

main = Main

#시간, 클릭 수 출력

class UI:
    def __init__(self):
        self.reset_font = load_font("resources/lazy_sunday_regular.ttf", 30)

        self.time_now = get_time()
        self.timer = 0
        self.timer_image = load_image('resources/timer.png')
        self.timer_font = load_font("resources/lazy_sunday_regular.ttf", 20)

        self.click = 0
        self.click_image = load_image('resources/click.png')
        self.click_font = load_font("resources/lazy_sunday_regular.ttf", 20)

    def update(self):
        self.timer = get_time()-self.time_now


    def draw(self):
        self.reset_font.draw(650, 50, 'r : reset', (233,170,73))
        self.timer_font.draw(100,120, '%d' % self.timer,(233,170,73))
        self.timer_image.draw(60,120)
        self.click_font.draw(100,60, '%d' % self.click,(233,170,73))
        self.click_image.draw(60,60)
