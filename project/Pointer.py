__author__ = 'SoYoung Lee'
from pico2d import *

class Pointer:
    def __init__(self):
        self.x = 400
        self.y = 300
        self.image = load_image("resources/pointer.png")
        self.change_time = 0
        self.frame = 0

    def update(self,frame_time):
        self.change_time += frame_time
        if self.change_time > 0.2:
            self.frame = (self.frame+1)%3
            self.change_time = 0

    def draw(self):
        self.image.clip_draw(self.frame*40, 0, 40, 40, self.x, self.y)