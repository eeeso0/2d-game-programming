__author__ = 'SoYoung Lee'

import GameFramework
import TitleState
from pico2d import *


name = "StartState"
image = None
bgm = None
logo_time = 0.0
playing = True

def enter():
    global image, bgm
    open_canvas()
    image = load_image('resources/kpu_credit.png')
    bgm = load_wav('sound/bgm.wav')



def exit():
    global image, bgm
    del(image)
    del(bgm)
    close_canvas()


def update(frame_time):
    global logo_time, playing

    if playing == True:
        if( logo_time > 1.0):
            logo_time = 0
            #game_framework.quit()
            GameFramework.push_state(TitleState)
            playing = False
        delay(0.01)
        logo_time += 0.01


def draw(frame_time):
    global image
    clear_canvas()
    image.draw(400,300)
    update_canvas()




def handle_events(frame_time):
    events = get_events()
    pass


def pause():
    global bgm
    bgm.set_volume(70)
    bgm.repeat_play()


def resume(): pass




