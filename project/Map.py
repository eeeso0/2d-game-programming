__author__ = 'SoYoung Lee'
from pico2d import*


class Map:
    def __init__(self):
        self.level = 1
        self.curr_level = 1
        self.level_image = load_image("resources/level.png")


        self.front_image = load_image("resources/map_front.png")
        self.back_image = load_image("resources/map_back.png")

        self.goal = [[0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0], [0,0,0,0,0]]


    def set_map(self):
        if self.curr_level == 1:
            self.goal = [[0, 0, 1, 0, 0],       #마름모
                        [0, 1, 0, 1, 0],
                        [1, 0, 0, 0, 1,],
                        [0, 1, 0, 1, 0],
                        [0, 0, 1, 0, 0]]
        elif self.curr_level == 2:
            self.goal = [[1, 0, 0, 0, 1],       # X모양
                        [0, 1, 0, 1, 0],
                        [0, 0, 1, 0, 0,],
                        [0, 1, 0, 1, 0],
                        [1, 0, 0, 0, 1]]
        elif self.curr_level == 3:
            self.goal = [[0, 1, 0, 1, 0],
                        [1, 0, 0, 0, 1],
                        [0, 0, 1, 0, 0,],
                        [1, 0, 0, 0, 1],
                        [0, 1, 0, 1, 0]]
        elif self.curr_level == 4:
            self.goal = [[1, 1, 0, 1, 1],
                        [1, 0, 1, 0, 1],
                        [0, 1, 0, 1, 0,],
                        [1, 0, 1, 0, 1],
                        [1, 1, 0, 1, 1]]
        elif self.curr_level == 5:
            self.goal = [[1, 0, 1, 0, 1],
                        [0, 1, 1, 1, 0],
                        [1, 1, 1, 1, 1,],
                        [0, 1, 1, 1, 0],
                        [1, 0, 1, 0, 1]]

    def draw(self):

        #레벨
        self.level_image.clip_draw((self.curr_level-1)*200, 0, 200, 100, 400, 500)

        #미니 맵
        for j in range(5):
            for i in range(5):
                if self.goal[j][i] == 0:
                    self.back_image.draw(700 + i*18, 500 + j*18)
                else:
                    self.front_image.draw(700 + i*18, 500 + j*18)